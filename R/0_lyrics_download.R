### # install.packages("genius")
library(genius)
library(dplyr)
selvatica <- genius_album("Karina Buhr", "Selvatica")
empv <- genius_album("Karina Buhr", "Eu menti pra voce")
desmanche <- genius_album("Karina Buhr", "Desmanche")
ldo <- genius_album("Karina Buhr", "Longe de onde")
# deveria ser algo assim:
#albums <- c("Selvatica", "Eu menti pra voce", "Desmanche", "Longe de onde")
#apply(albums, genius_album)

karina <- bind_rows(empv, ldo, selvatica, desmanche)
karina
write.csv(karina, "./data/mercado_fonografico.csv")

#To see a song
selvatica %>%
  filter(track_title == "Pic Nic") %>%
  View()

#to get a random phrase
(random <- sample(karina$lyric, 1))

# context of the phrase- take a couple verses before and after
pattern <- random #aqui é length == 1
index <- which(karina$lyric == pattern)
context <- 1 #quantas frases mais ou menos
#ainda um apply mas pattern, index e context devem ser parâmetros de uma função
sapply(index, function(x) {
  paste(karina$lyric[seq(x - context, x + context,1)], collapse = "/")
}) %>%
  unique()

# approximate grep - tem um parâmetro distancia que não estou mexendo
patterns <- karina$lyric[grep("começou", karina$lyric)]
#isto pode ter length > 1 por isso patterns
index <- which(karina$lyric %in% patterns)
context <- 1
sapply(index, function(x) {
  paste(karina$lyric[seq(x - context, x + context,1)], collapse = "/")
}) %>%
  unique()

#dictionary - to copiando e colando coisas aleatorias aqui
# "Desapego corretamente"
# "Quem não sabe desistir" #context + 1
# "Vive um tanto complicado"
# "Que não é choro" # -1
# "Precisando exercitar paz e amor"
# "Não faz quem não acredita"
# "Cada linha é obra prima"
# "A casa caiu!"
# "Na versão da vez do dono"
# "Pode morrer aqui"
# "Com aplausos do público"

